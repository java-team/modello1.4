#!/bin/sh -e

VERSION=$2
TAR=../modello1.4_$VERSION.orig.tar.xz
DIR=modello-$VERSION
TAG=modello-$2

mkdir -p $DIR
tar --strip=1 -xvf $3 -C $DIR
rm $3
rm -rf $DIR/modello-maven-plugin
rm -rf $DIR/modello-test
tar -c -J -v -f $TAR $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi
